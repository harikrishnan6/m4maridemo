package com.marriage.m4matri.responses

data class LoginResponse(
    val access_token: String?,
    val expires_in: Int?,
    val token_type: String?,
    val refresh_token: String?,
    val state: String?,
    val deviceToken: String?
)

data class ErrorCode(val error: ErrorData?)

data class ErrorData(
    val code: Int?, val type: String?,
    val message: String?
)
