package com.marriage.m4matri.ui.base

import android.content.Intent
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.lifecycle.lifecycleScope
import androidx.paging.CombinedLoadStates
import androidx.paging.LoadState
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.marriage.m4matri.adapter.MatriLoadStateAdapter
import com.marriage.m4matri.adapter.MatriProfileAdapter
import com.marriage.m4matri.databinding.ActivityHomepageBinding
import com.marriage.m4matri.di.UserDatabase
import com.marriage.m4matri.ui.LoginActivity
import com.marriage.m4matri.userpreferences.UserPreferences
import kotlinx.coroutines.*
import javax.inject.Inject

abstract class BaseDashboardActivity : AppCompatActivity()
{
    lateinit var binding: ActivityHomepageBinding
    val adapter by lazy { MatriProfileAdapter() }
    @Inject
    lateinit var userPreferences: UserPreferences
    @Inject
    lateinit var userDatabase: UserDatabase

    fun initAdapter(isMediator: Boolean = false) {
        binding.rvData.adapter = adapter.withLoadStateHeaderAndFooter(
            header = MatriLoadStateAdapter { adapter.retry() },
            footer = MatriLoadStateAdapter { adapter.retry() }
        )
        adapter.addLoadStateListener { loadState ->
            val refreshState = if (isMediator) {
                loadState.mediator?.refresh
            } else {
                loadState.source.refresh
            }
            binding.rvData.isVisible = refreshState is LoadState.NotLoading
            binding.progressBar.isVisible = refreshState is LoadState.Loading
            binding.buttonRetry.isVisible = refreshState is LoadState.Error
            handleError(loadState)
        }
        binding.buttonRetry.setOnClickListener {
            adapter.retry()
        }
        binding.ivLogout.setOnClickListener {
            runBlocking {
                val result = lifecycleScope.async(Dispatchers.IO) {
                    userPreferences.clear()
                    userDatabase.getProfileDao().deleteAll()
                    userDatabase.getRemoteKeyDao().deleteAll()
                }
                result.await()
            }
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
            finish()
        }
    }

    private fun handleError(loadState: CombinedLoadStates) {
        val errorState = loadState.source.append as? LoadState.Error ?: loadState.source.prepend as? LoadState.Error
        errorState?.let {
            Toast.makeText(this, "${it.error}", Toast.LENGTH_LONG).show()
        }
    }

}