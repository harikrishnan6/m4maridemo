package com.marriage.m4matri.ui

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.lifecycle.Observer
import androidx.lifecycle.asLiveData
import androidx.paging.ExperimentalPagingApi
import com.marriage.m4matri.R
import com.marriage.m4matri.helper.TOKEN
import com.marriage.m4matri.userpreferences.UserPreferences
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
@SuppressLint("CustomSplashScreen")
class SplashActivity : AppCompatActivity() {

    @Inject
    lateinit var userPreferences: UserPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        userCheck()
    }

    private fun userCheck() {
        Handler(Looper.getMainLooper()).postDelayed({
            checkUserExist()
        }, 3000)
    }

    private fun checkUserExist() {
        userPreferences.authToken.asLiveData().observe(this, Observer {
            if (it != null && it.isNotEmpty()) {
                navHomePage(it)
            } else {
                navLoginPage()
            }
        })
    }

    @OptIn(ExperimentalPagingApi::class)
    private fun navHomePage(token: String) {
        val intent = Intent(this, HomepageActivity::class.java)
        intent.putExtra(TOKEN, token)
        startActivity(intent)
        finish()
    }

    private fun navLoginPage() {
        val logIntent = Intent(this, LoginActivity::class.java)
        startActivity(logIntent)
        finish()
    }
}