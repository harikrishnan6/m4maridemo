package com.marriage.m4matri.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.marriage.m4matri.R
import com.marriage.m4matri.databinding.MatriItemsLayoutBinding
import com.marriage.m4matri.helper.PackageName
import com.marriage.m4matri.helper.StatusCheck
import com.marriage.m4matri.helper.visible
import com.marriage.m4matri.responses.UserList
import java.util.*


class MatriProfileAdapter() : PagingDataAdapter<UserList, MatriProfileAdapter.MatriProfileView>(DIF_CALLBACK) {


    companion object {
        val DIF_CALLBACK = object : DiffUtil.ItemCallback<UserList>() {
            override fun areItemsTheSame(oldItem: UserList, newItem: UserList): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: UserList, newItem: UserList): Boolean {
                return oldItem == newItem
            }
        }
    }


    class MatriProfileView(val binding: MatriItemsLayoutBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: UserList?, position: Int) {
            when (position) {
                1-> {
                    binding.cardContent.visible(true)
                    binding.cardProfile.visible(false)
                }
                else -> {
                    binding.cardContent.visible(false)
                    binding.cardProfile.visible(true)

                    binding.apply {
                        ivUserProfile.load(item?.profileImages) {
                            placeholder(R.drawable.ic_baseline_person_24)
                            error(R.drawable.ic_baseline_person_24)
                        }
                        setTextContents(this, item)

                        when (item?.identityBadge) {
                            StatusCheck.DISABLED.status -> {
                                ivActiveStatus.setImageResource(R.drawable.layer_circle_gray)
                            }
                            else -> {
                                ivActiveStatus.setImageResource(R.drawable.layer_circle_green)
                            }
                        }
                        when (item?.favourite) {
                            StatusCheck.DISABLED.status  -> {
                                ivFavourites.backgroundTintList=ContextCompat.getColorStateList(itemView.context,R.color.white)
                            }
                            else -> {
                                ivFavourites.backgroundTintList=ContextCompat.getColorStateList(itemView.context,R.color.purple_200)
                            }
                        }
                        when(item?.packageName){
                            PackageName.ACTIVE_PLUS.content->{
                                flActPremium.visible(true)
                                flActPremium.backgroundTintList=ContextCompat.getColorStateList(itemView.context,R.color.purple_500)
                            }
                            PackageName.PREMIUM.content,PackageName.PREMIUM_PLUS.content->{
                                flActPremium.visible(true)
                                flActPremium.backgroundTintList=ContextCompat.getColorStateList(itemView.context,R.color.purple_200)
                            }
                            else->{
                                flActPremium.visible(false)
                            }
                        }

                    }
                }
            }
        }

        private fun setTextContents(binding: MatriItemsLayoutBinding, item: UserList?) {
            binding.tvId.text = item?.id
            binding.tvProfileName.text = item?.name + ","
            binding.tvAge.text = item?.age + "Yrs"
            binding.tvArtistProfession.text = item?.occupation
            binding.tvLocation.text = item?.location
            binding.tvPremiumActive.text= item?.packageName?.replaceFirstChar {
                if (it.isLowerCase()) it.titlecase(Locale.getDefault()
                ) else it.toString()
            }
        }
    }

    override fun onBindViewHolder(holder: MatriProfileView, position: Int) {
        val item = getItem(position)
        holder.bind(item, position)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MatriProfileView {
        val binding = MatriItemsLayoutBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MatriProfileView(binding)

    }
}