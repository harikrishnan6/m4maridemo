package com.marriage.m4matri.viewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.ExperimentalPagingApi
import androidx.paging.cachedIn
import com.marriage.m4matri.repository.MatRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * @Hiltviewmodel will make the models to be created using Hilts model factory
 * @Inject annotation used to inject all dependencies to viewmodel class
 */


@ExperimentalPagingApi
@HiltViewModel
class NetworkAndDbViewModel @Inject constructor(private val repository: MatRepository) :
    ViewModel() {

    val dataSource = repository.getProfileFromMediator().cachedIn(viewModelScope)
}
