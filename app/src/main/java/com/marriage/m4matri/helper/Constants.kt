package com.marriage.m4matri.helper

const val CLIENT_ID="M4marry.e803e03342714f29db81a16bb1049dca0521dc084"
const val GRANT_TYPE="password"
const val STATE_USER="2434523"
const val MATRI_DATA_STORE="matri_data_store"
const val AUTH_KEY="auth_key"
const val REFRESH_KEY="refresh_key"
const val USER_NAME="user_name"
const val PASSWORD="password"
const val LIST_TYPE="array"
const val TOKEN="Token"

const val PAGE_SIZE=12
