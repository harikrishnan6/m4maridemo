package com.marriage.m4matri.repository

import androidx.paging.*
import com.marriage.m4matri.di.UserDatabase
import com.marriage.m4matri.helper.PAGE_SIZE
import com.marriage.m4matri.network.ApiService
import com.marriage.m4matri.network.UserMediator
import com.marriage.m4matri.network.UsersPagingSource
import com.marriage.m4matri.responses.ProfileData
import com.marriage.m4matri.responses.UserList
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class MatRepository @Inject constructor(private val  apiService: ApiService,private val userDatabase: UserDatabase)
{

    @ExperimentalPagingApi
     fun getProfileFromMediator(): Flow<PagingData<UserList>>{

        val pagingSourceFactory= {userDatabase.getProfileDao().getAll()}
        return Pager(
            config = PagingConfig(
                pageSize = PAGE_SIZE,
                maxSize = PAGE_SIZE + (PAGE_SIZE * 2),
                enablePlaceholders = false,
            ),
            remoteMediator = UserMediator(
                apiService,
                userDatabase
            ),
            pagingSourceFactory = pagingSourceFactory
        ).flow

    }

    fun getProfileFromNetworkSourceOnly():Flow<PagingData<UserList>>{
        return Pager(
            config = PagingConfig(
                pageSize = PAGE_SIZE,
                maxSize = PAGE_SIZE + (PAGE_SIZE * 2),
                enablePlaceholders = false
            ),
            pagingSourceFactory = { UsersPagingSource(apiService, database = userDatabase) }
        ).flow
    }

}