package com.marriage.m4matri.network

import com.marriage.m4matri.responses.ProfileData
import com.marriage.m4matri.responses.UserList

fun checkMatridExist(userDetails: List<ProfileData>?): ArrayList<UserList> {
    val usersList = ArrayList<UserList>()
    if (!userDetails.isNullOrEmpty()) {
        for (i in userDetails.indices) {
            if (!userDetails[i].id.isNullOrEmpty()) {
                usersList.add(
                    UserList(id = userDetails[i].id!!,
                        name = userDetails[i].name,
                        gender = userDetails[i].gender,
                        age = userDetails[i].age,
                        profileThumbImages = userDetails[i].profileThumbImages,
                        profileImages = userDetails[i].profileImages,
                        education = userDetails[i].education,
                        occupation = userDetails[i].occupation,
                        packageName = userDetails[i].packageName,
                        profileStatus = userDetails[i].profileStatus,
                        identityBadge = userDetails[i].identityBadge,
                        country = userDetails[i].country,
                        location = userDetails[i].location,
                        favourite = userDetails[i].favourite,
                        activePlusUser = userDetails[i].activePlusUser
                    )
                )
            }
        }
    }
    return usersList
}
