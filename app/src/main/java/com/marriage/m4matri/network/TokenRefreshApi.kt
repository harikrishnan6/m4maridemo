package com.marriage.m4matri.network

import com.marriage.m4matri.responses.LoginResponse
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface TokenRefreshApi {

    @FormUrlEncoded
    @POST("authorize/token")
    suspend fun refreshToken(
        @Field("client_id") client_id: String,
        @Field("grant_type") grant_type: String,
        @Field("state") state: String,
        @Field("password") password: String,
        @Field("username") username: String
    ): LoginResponse
}