package com.marriage.m4matri.di

import androidx.paging.PagingSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.marriage.m4matri.responses.ProfileData
import com.marriage.m4matri.responses.UserList

/**
 * Dao is used to access the application persisted data
 * @Insert Room generates an implementation that insert all parameters into the database
 * @OnConflictStrategy.REPLACE for replacing old data and continue transaction
 */
@Dao
interface ProfileDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertALl(profiles: List<UserList>)

    @Query("SELECT * FROM users")
    fun getAll(): PagingSource<Int, UserList>

    @Query("DELETE FROM users")
    suspend fun deleteAll()
}