package com.marriage.m4matri.di

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.marriage.m4matri.responses.RemoteKeys

/**
 * Dao is used to access the application persisted data
 * @Insert Room generates an implementation that insert all parameters into the database
 * @OnConflictStrategy.REPLACE for replacing old data and continue transaction
 */
@Dao
interface RemoteKeyDao
{
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(remoteKeys:List<RemoteKeys>)

    @Query("SELECT * FROM remote_keys where id =:mat_id")
    suspend fun remoteKeyCatId(mat_id:String):RemoteKeys?

    @Query("DELETE FROM remote_keys")
    suspend fun deleteAll()

}