package com.marriage.m4matri.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.paging.LoadState
import androidx.paging.LoadStateAdapter
import androidx.recyclerview.widget.RecyclerView
import com.marriage.m4matri.databinding.ItemLoadStateBinding


class MatriLoadStateAdapter(private val retry: () -> Unit) : LoadStateAdapter<MatriLoadStateAdapter.MatriLoadStateViewHolder>() {


    class MatriLoadStateViewHolder(val binding: ItemLoadStateBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(loadState: LoadState, retry: () -> Unit)
        {
            if (loadState is LoadState.Error) {
                binding.errorMsg.text = loadState.error.localizedMessage
            }
            binding.progressBar.isVisible = loadState is LoadState.Loading
            binding.retryButton.isVisible = loadState !is LoadState.Loading
           // binding.errorMsg.isVisible = loadState !is LoadState.Loading
            binding.retryButton.setOnClickListener {
                retry.invoke()
            }
        }

    }

    override fun onBindViewHolder(holder: MatriLoadStateViewHolder, loadState: LoadState) {
        holder.bind(loadState, retry)
    }

    override fun onCreateViewHolder(parent: ViewGroup, loadState: LoadState): MatriLoadStateViewHolder {
        val binding = ItemLoadStateBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MatriLoadStateViewHolder(binding)
    }
}