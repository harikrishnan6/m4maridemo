package com.marriage.m4matri.responses

import androidx.room.Entity
import androidx.room.PrimaryKey

data class HomepageResponses(
    val profiles: List<ProfileData>?,
    val total: Int?, val totalPages: Int?, val count: Int?, val page: Int?
)


data class ProfileData(
    val id: String?,
    val name: String?,
    val gender: String?,
    val age: String?,
    val profileThumbImages: String?,
    val profileImages: String?,
    val education: String?,
    val occupation: String?,
    val packageName: String?,
    val profileStatus: String?,
    val identityBadge: Int?,
    val country: String?="",
    val location: String?="",
    val favourite:Int?,
    val activePlusUser:Boolean?
)