package com.marriage.m4matri.repository

import android.util.Log
import com.marriage.m4matri.network.Resource
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.HttpException

/**
 * This class actually call our api and return success or failure result
 * If the result is successful we map it to the success part of enum, for all other conditions
 * such as network error or http exception we map to the failure part of enum class
 * Here we return the result as Resource to the invoking repo class.
 */
abstract class BaseRepository {

    suspend fun <T> safeApiCall(apiCall: suspend () -> T): Resource<T> {
        return withContext(Dispatchers.IO)
        {
            try {
                Resource.Success(apiCall.invoke())
            } catch (throwable: Throwable) {
                when (throwable) {
                    is HttpException -> {
                        Resource.Failure(false, throwable.code(), throwable.response()?.errorBody())
                    }
                    else -> {
                        Resource.Failure(true, null, null)
                    }
                }
            }
        }
    }
}