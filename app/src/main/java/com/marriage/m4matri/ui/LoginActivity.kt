package com.marriage.m4matri.ui

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.addTextChangedListener
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.paging.ExperimentalPagingApi
import com.marriage.m4matri.R
import com.marriage.m4matri.databinding.ActivityLoginBinding
import com.marriage.m4matri.helper.*
import com.marriage.m4matri.network.Resource
import com.marriage.m4matri.responses.LoginResponse
import com.marriage.m4matri.userpreferences.UserPreferences
import com.marriage.m4matri.viewModel.AuthViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import javax.inject.Inject

@AndroidEntryPoint
class LoginActivity : AppCompatActivity() {

    private val viewModel:AuthViewModel by viewModels()
    @Inject lateinit var userPreferences: UserPreferences
    private lateinit var binding: ActivityLoginBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding= DataBindingUtil.setContentView(this,R.layout.activity_login)
        setClickListeners()
        setEditTextListeners()
        setObservers()
    }

    private fun setEditTextListeners() {
        displayTitleText(binding.tvTitle,this)
        binding.btnSign.isEnabled(false)
        binding.etPassword.addTextChangedListener {
            val email=binding.etUsername.text.toString().trim()
            binding.btnSign.isEnabled(email.isNotEmpty() && it.toString().trim().isNotEmpty())
        }
    }



    @OptIn(ExperimentalPagingApi::class)
    private fun navHomePage(token: String) {
        val intent = Intent(this, HomepageActivity::class.java)
        intent.putExtra(TOKEN,token)
        startActivity(intent)
        finish()
    }


    private fun setObservers() {
        viewModel.loginResponse.observe(this, Observer {
            binding.progress.visible(false)
            when (it) {
                is Resource.Success -> {
                    saveData(it.value)
                }
                is Resource.Failure -> {
                    handleApiError(it)
                }
            }
        })
    }

    private fun saveData(loginResponse: LoginResponse) {
        val userName = binding.etUsername.text.toString().trim()
        val password = binding.etPassword.text.toString().trim()
        lifecycleScope.launch {
            loginResponse.access_token?.let {
                viewModel.saveAuthDetails(userName, password, it)
                navHomePage(it)
            }
        }
    }


    private fun setClickListeners() {
        binding.btnSign.setOnClickListener {
            binding.progress.visible(true)
            viewModel.login(
                binding.etUsername.text.toString().trim(),
                binding.etPassword.text.toString().trim()
            )
        }
    }
}