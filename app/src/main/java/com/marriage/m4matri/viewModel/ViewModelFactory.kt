package com.marriage.m4matri.viewModel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.marriage.m4matri.repository.AuthRepository
import com.marriage.m4matri.repository.BaseRepository

class ViewModelFactory(private val repository: BaseRepository): ViewModelProvider.NewInstanceFactory()
{

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return when {
            modelClass.isAssignableFrom(AuthViewModel::class.java) -> {
                AuthViewModel(repository as AuthRepository) as T
            }
            else -> {
                throw IllegalArgumentException("View model class not found")
            }
        }
    }
}