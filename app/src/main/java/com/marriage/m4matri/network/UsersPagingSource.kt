package com.marriage.m4matri.network

import androidx.paging.PagingSource
import androidx.paging.PagingState
import com.marriage.m4matri.di.UserDatabase
import com.marriage.m4matri.helper.LIST_TYPE
import com.marriage.m4matri.responses.UserList
import retrofit2.HttpException
import java.io.IOException

class UsersPagingSource(private val apiService: ApiService, private val database: UserDatabase) :
    PagingSource<Int, UserList>()
{
    override fun getRefreshKey(state: PagingState<Int, UserList>): Int? {
        return state.anchorPosition?.let { anchorPosition ->
            state.closestPageToPosition(anchorPosition)?.prevKey?.plus(1)
                ?: state.closestPageToPosition(anchorPosition)?.nextKey?.minus(1)
        }
    }

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, UserList> {
        val page = params.key ?: STARTING_PAGE_INDEX
        return try {
            val response = apiService.getUsersList(listType = LIST_TYPE, offset = page.toString())
            val userList = checkMatridExist(response.profiles)
            LoadResult.Page(
                data = userList,
                prevKey = if (page == STARTING_PAGE_INDEX) null else page - 1,
                nextKey = if (userList.isEmpty()) null else page + 1
            )
        } catch (exception: IOException) {
            return LoadResult.Error(exception)
        } catch (exception: HttpException) {
            return LoadResult.Error(exception)
        }
    }

}