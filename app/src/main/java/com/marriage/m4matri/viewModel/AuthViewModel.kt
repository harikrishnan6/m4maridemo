package com.marriage.m4matri.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.marriage.m4matri.network.Resource
import com.marriage.m4matri.repository.AuthRepository
import com.marriage.m4matri.responses.HomepageResponses
import com.marriage.m4matri.responses.LoginResponse
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class AuthViewModel @Inject constructor(private val repository: AuthRepository):ViewModel()
{
    private val _loginResponse:MutableLiveData<Resource<LoginResponse>> = MutableLiveData()
    val loginResponse:LiveData<Resource<LoginResponse>> get()=_loginResponse

    private val _userResponse:MutableLiveData<Resource<HomepageResponses>> = MutableLiveData()
    val userResponse:LiveData<Resource<HomepageResponses>> get() = _userResponse

    fun login(username:String,password:String){
        viewModelScope.launch {
            _loginResponse.value=repository.login(username,password)
        }
    }

    fun getUserDetails(offset:String){
        viewModelScope.launch {
            _userResponse.value=repository.getUsersList(offset)
        }
    }

    suspend fun saveAuthDetails(username: String,password: String,token:String){
        viewModelScope.launch {
            repository.saveAuthTokenUserDetails(token,username,password)
        }
    }


}