package com.marriage.m4matri.di

import android.content.Context
import androidx.room.Room
import com.marriage.m4matri.network.ApiService
import com.marriage.m4matri.network.RemoteDataSource
import com.marriage.m4matri.network.TokenRefreshApi
import com.marriage.m4matri.userpreferences.UserPreferences
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

/**
 *@Module annotation which will make this act as a module
 * to inject dependency to other class within its scope
 * @Installin(SingletonCompnent::class) this will make this class to inject dependency to entire app.
 */
@Module
@InstallIn(SingletonComponent::class)
object AppModule
{
    @Singleton
    @Provides
    fun provideAuthApi(@ApplicationContext context: Context,remoteDataSource: RemoteDataSource):ApiService
    {
        return remoteDataSource.buildApi2(ApiService::class.java, context)
    }

    @Singleton
    @Provides
    fun provideUserApi(remoteDataSource: RemoteDataSource, @ApplicationContext context: Context): TokenRefreshApi {
        return remoteDataSource.buildApi2(TokenRefreshApi::class.java, context)
    }

    @Singleton
    @Provides
    fun provideUserPreferences(@ApplicationContext context: Context): UserPreferences {
        return UserPreferences(context)
    }

    @Singleton
    @Provides
    fun provideDb(@ApplicationContext context: Context):UserDatabase{
        return Room.databaseBuilder(context,UserDatabase::class.java,"User.db").build()
    }
}