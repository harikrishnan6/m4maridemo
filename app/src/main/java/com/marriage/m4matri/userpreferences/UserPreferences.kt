package com.marriage.m4matri.userpreferences

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.clear
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.preferencesKey
import androidx.datastore.preferences.createDataStore
import com.marriage.m4matri.helper.*
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject


class UserPreferences @Inject constructor(@ApplicationContext context: Context)
{

    private val appcontext = context.applicationContext
    private val datastore: DataStore<Preferences> = appcontext.createDataStore(name = MATRI_DATA_STORE)

    val authToken: Flow<String?>
        get() = datastore.data.map { preferences ->
            preferences[KEY_AUTH]
        }

    suspend fun saveAuthToken(authToken: String) {
        datastore.edit { preferences ->
            preferences[KEY_AUTH] = authToken
        }
    }


    val authUserName: Flow<String?>
        get() = datastore.data.map { preferences ->
            preferences[KEY_USER]
        }

    suspend fun saveAuthUser(authUser: String) {
        datastore.edit { preferences ->
            preferences[KEY_USER] = authUser
        }
    }

    val authPassword: Flow<String?>
        get() = datastore.data.map { preferences ->
            preferences[KEY_PASSWORD]
        }

    suspend fun saveAuthPassword(authPassword: String) {
        datastore.edit { preferences ->
            preferences[KEY_PASSWORD] = authPassword
        }
    }

    suspend fun clear(){
        datastore.edit { preference->
            preference.clear()
        }
    }


    companion object {
        private val KEY_AUTH = preferencesKey<String>(AUTH_KEY)
        private val KEY_USER = preferencesKey<String>(USER_NAME)
        private val KEY_PASSWORD = preferencesKey<String>(PASSWORD)
    }
}