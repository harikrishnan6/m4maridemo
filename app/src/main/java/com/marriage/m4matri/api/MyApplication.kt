package com.marriage.m4matri.api

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

/**
 * Hilt Application class
 */
@HiltAndroidApp
class MyApplication:Application()
{

}