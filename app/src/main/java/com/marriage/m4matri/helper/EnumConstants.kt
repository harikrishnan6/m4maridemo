package com.marriage.m4matri.helper

enum class StatusCheck(val status:Int){
    ACTIVE(1),
    DISABLED(0)
}

enum class PackageName(val content:String){
    FREE("free"),
    ACTIVE_PLUS("Active Plus"),
    PREMIUM("Premium"),
    PREMIUM_PLUS("Premium Plus")
}