package com.marriage.m4matri.helper

import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.graphics.Color
import android.text.Spannable
import android.text.SpannableString
import android.text.style.ForegroundColorSpan
import android.view.View
import android.widget.Toast
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import com.google.android.material.snackbar.Snackbar
import com.marriage.m4matri.R
import com.marriage.m4matri.network.Resource
import com.marriage.m4matri.ui.LoginActivity


fun View.visible(isViewVisible: Boolean) {
    visibility = if (isViewVisible) View.VISIBLE else View.GONE
}

fun View.isEnabled(enabled: Boolean) {
    isEnabled = enabled
    alpha = if (enabled) 1f else 0.5f
}



fun View.snackBar(message: String, action: (() -> Unit)? = null) {
    val snackbar = Snackbar.make(this, message, Snackbar.LENGTH_LONG)
    action?.let {
        snackbar.setAction("Retry") {
            it()
        }
    }
    snackbar.show()
}

fun Activity.handleApiError(failure: Resource.Failure, retry:(()->Unit)?=null)
{
    when{
        failure.isNetworkError->{
            displayAlertMessage(getString(R.string.connect_internet),this)
        }
        failure.errorCode == 401->{
            Toast.makeText(this,getString(R.string.auth_blocked),Toast.LENGTH_LONG).show()
        }
        failure.errorCode==400->{
            if(this is LoginActivity) {
                displayAlertMessage(getString(R.string.incorrect_username_password),this)
            }else{
                displayAlertMessage(failure.errorBody.toString(),this)
            }
        }
    }
}

fun displayAlertMessage(message:String,context: Context){
    val alertMessage=AlertDialog.Builder(context)
    alertMessage.setTitle("Alert")
    alertMessage.setMessage(message)
    alertMessage.setCancelable(false)
    alertMessage.setPositiveButton("Ok") { dialog, which ->
        dialog.dismiss()
    }
    val builder=alertMessage.create()
    builder.show()
}

fun displayTitleText(textView:AppCompatTextView,context: Context){
    val word: Spannable = SpannableString("m")
    word.setSpan( ForegroundColorSpan(ContextCompat.getColor(context,R.color.purple_500)), 0, word.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
    textView.text = word
    val word2: Spannable = SpannableString("4")
    word2.setSpan( ForegroundColorSpan(ContextCompat.getColor(context,R.color.purple_200)), 0, word2.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
    textView.append(word2)
    val word3: Spannable = SpannableString("marry.com")
    word3.setSpan( ForegroundColorSpan(ContextCompat.getColor(context,R.color.purple_500)), 0, word3.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
    textView.append(word3)
}
