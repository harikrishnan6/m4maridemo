package com.marriage.m4matri.ui

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil

import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.paging.CombinedLoadStates
import androidx.paging.ExperimentalPagingApi
import androidx.paging.LoadState
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.marriage.m4matri.R
import com.marriage.m4matri.adapter.MatriLoadStateAdapter
import com.marriage.m4matri.adapter.MatriProfileAdapter
import com.marriage.m4matri.databinding.ActivityHomepageBinding
import com.marriage.m4matri.helper.displayTitleText
import com.marriage.m4matri.network.Resource
import com.marriage.m4matri.ui.base.BaseActivity
import com.marriage.m4matri.ui.base.BaseDashboardActivity
import com.marriage.m4matri.userpreferences.UserPreferences
import com.marriage.m4matri.viewModel.AuthViewModel
import com.marriage.m4matri.viewModel.NetworkAndDbViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import javax.inject.Inject

@ExperimentalPagingApi
@AndroidEntryPoint
class HomepageActivity : BaseDashboardActivity() {

    /**
     * ViewModels delegate used to get
     * byViewModels will automatically construct the viewmodel
     */
    private val viewModel: NetworkAndDbViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_homepage)
        initAdapter()
        displayTitleText(binding.tvTitle,this)
        lifecycleScope.launch{
            viewModel.dataSource.collectLatest {
                adapter.submitData(it)
            }
        }
    }


}