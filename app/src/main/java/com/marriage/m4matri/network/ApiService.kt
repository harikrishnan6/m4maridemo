package com.marriage.m4matri.network

import com.marriage.m4matri.responses.HomepageResponses
import com.marriage.m4matri.responses.LoginResponse
import retrofit2.http.*

interface ApiService {

    @FormUrlEncoded
    @POST("authorize/token")
    suspend fun loginUser(
        @Field("client_id") client_id: String,
        @Field("grant_type") grant_type: String,
        @Field("state") state: String,
        @Field("password") password: String,
        @Field("username") username: String
    ): LoginResponse

    @GET("v4/profilesVisited/me")
    suspend fun getUsersList(
        @Query("listType") listType: String,
        @Query("offset") offset: String
    ): HomepageResponses


}