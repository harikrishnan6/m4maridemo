package com.marriage.m4matri.network

import androidx.paging.ExperimentalPagingApi
import androidx.paging.LoadType
import androidx.paging.PagingState
import androidx.paging.RemoteMediator
import androidx.room.withTransaction
import com.marriage.m4matri.di.UserDatabase
import com.marriage.m4matri.helper.LIST_TYPE
import com.marriage.m4matri.repository.AuthRepository
import com.marriage.m4matri.responses.HomepageResponses
import com.marriage.m4matri.responses.ProfileData
import com.marriage.m4matri.responses.RemoteKeys
import com.marriage.m4matri.responses.UserList
import okio.IOException
import retrofit2.HttpException
import java.util.concurrent.TimeUnit
import javax.inject.Inject

const val STARTING_PAGE_INDEX = 1

@ExperimentalPagingApi
class UserMediator(private val apiService: ApiService, private val database: UserDatabase) :
    RemoteMediator<Int, UserList>() {


    override suspend fun initialize(): InitializeAction {
        return InitializeAction.LAUNCH_INITIAL_REFRESH
    }

    override suspend fun load(loadType: LoadType, state: PagingState<Int, UserList>): MediatorResult {
        val pagingKeyData = getKeyPageData(loadType, state)
        val page = when (pagingKeyData) {
            is MediatorResult.Success -> {
                return pagingKeyData
            }
            else -> {
                pagingKeyData as Int
            }
        }
        try {
            val response = apiService.getUsersList(LIST_TYPE, offset = page.toString())
            val userList=checkMatridExist(response.profiles)
            val isEndofList =userList.isEmpty()
            database.withTransaction {
                if(loadType == LoadType.REFRESH){
                    database.getProfileDao().deleteAll()
                    database.getRemoteKeyDao().deleteAll()
                }
                val prevKey = if (page == STARTING_PAGE_INDEX) null else page - 1
                val nextKey = if (isEndofList) null else page + 1
                val remoteKeys=userList.map {
                    RemoteKeys(id=it.id, prevKey = prevKey, nextKey = nextKey)
                }
                database.getProfileDao().insertALl(userList)
                database.getRemoteKeyDao().insertAll(remoteKeys)
            }
            return MediatorResult.Success(endOfPaginationReached = isEndofList)

        } catch (e: IOException) {
            return MediatorResult.Error(e)
        } catch (e: HttpException) {
            return MediatorResult.Error(e)
        }
    }



    private suspend fun getKeyPageData(loadType: LoadType, state: PagingState<Int, UserList>): Any {
        return when (loadType) {
            LoadType.REFRESH -> {
                val remoteKeys = getRemoteKeyClosestToCurrentPosition(state)
                remoteKeys?.nextKey?.minus(1) ?: STARTING_PAGE_INDEX
            }
            LoadType.APPEND -> {
                val remoteKeys = getLastRemoteKey(state)
                val nextKey = remoteKeys?.nextKey
                return nextKey ?: MediatorResult.Success(endOfPaginationReached = false)
            }
            LoadType.PREPEND -> {
                val remoteKey = getFirstRemoteKey(state)
                val prevKey = remoteKey?.prevKey ?: MediatorResult.Success(endOfPaginationReached = false)
                prevKey
            }
        }
    }

    private suspend fun getRemoteKeyClosestToCurrentPosition(state: PagingState<Int, UserList>): RemoteKeys? {
        return state.anchorPosition?.let { position ->
            state.closestItemToPosition(position)?.id?.let { repoId ->
                database.getRemoteKeyDao().remoteKeyCatId(repoId)
            }
        }
    }

    private suspend fun getLastRemoteKey(state: PagingState<Int, UserList>): RemoteKeys? {
        return state.pages.lastOrNull { it.data.isNotEmpty() }?.data?.lastOrNull()
            ?.let { cat -> database.getRemoteKeyDao().remoteKeyCatId(cat.id) }
    }

    private suspend fun getFirstRemoteKey(state: PagingState<Int, UserList>): RemoteKeys? {
        return state.pages.firstOrNull { it.data.isNotEmpty() }?.data?.firstOrNull()?.let { cat ->
            database.getRemoteKeyDao().remoteKeyCatId(cat.id)
        }
    }

}