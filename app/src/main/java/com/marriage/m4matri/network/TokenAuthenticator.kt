package com.marriage.m4matri.network

import android.content.Context
import android.media.session.MediaSession.Token
import com.marriage.m4matri.helper.*
import com.marriage.m4matri.repository.BaseRepository
import com.marriage.m4matri.responses.LoginResponse
import com.marriage.m4matri.userpreferences.UserPreferences
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.runBlocking
import okhttp3.Authenticator
import okhttp3.Request
import okhttp3.Response
import okhttp3.Route

class TokenAuthenticator(context: Context,private val tokenApi: TokenRefreshApi):Authenticator,BaseRepository()
{
    private val appContext = context.applicationContext
    private val userPreferences = UserPreferences(appContext)

    override fun authenticate(route: Route?, response: Response): Request? {
        return runBlocking {
            when(val tokenResponse = getUpdatedToken()){
                is Resource.Success->{
                    tokenResponse.value.access_token?.let {
                        userPreferences.saveAuthToken(it)
                    }
                    response.request.newBuilder()
                        .header("Authorization", "Bearer ${tokenResponse.value.access_token}")
                        .build()
                }
                else->{
                    null
                }
            }
        }
    }

    private suspend fun getUpdatedToken(): Resource<LoginResponse> {
        val userName = userPreferences.authUserName.first()
        val password= userPreferences.authPassword.first()
        return safeApiCall { tokenApi.refreshToken(CLIENT_ID, GRANT_TYPE, STATE_USER, password?:"",
            userName?:"")}
    }
}