package com.marriage.m4matri.repository

import com.marriage.m4matri.helper.CLIENT_ID
import com.marriage.m4matri.helper.GRANT_TYPE
import com.marriage.m4matri.helper.LIST_TYPE
import com.marriage.m4matri.helper.STATE_USER
import com.marriage.m4matri.network.ApiService
import com.marriage.m4matri.network.Resource
import com.marriage.m4matri.responses.HomepageResponses
import com.marriage.m4matri.responses.LoginResponse
import com.marriage.m4matri.userpreferences.UserPreferences
import kotlinx.coroutines.flow.first
import javax.inject.Inject

class AuthRepository @Inject constructor(private val api: ApiService,private val preferences: UserPreferences) : BaseRepository()
{

    suspend fun login(username: String, password: String): Resource<LoginResponse> {
        return safeApiCall {
            api.loginUser(
                client_id = CLIENT_ID, grant_type = GRANT_TYPE, state = STATE_USER,
                password = password, username = username
            )
        }
    }

    suspend fun getUsersList(offset:String):Resource<HomepageResponses> {
        return safeApiCall {
            api.getUsersList(LIST_TYPE,"1")
        }
    }



    suspend fun saveAuthTokenUserDetails(token:String,username: String,password: String){
        preferences.saveAuthUser(username)
        preferences.saveAuthPassword(password)
        preferences.saveAuthToken(token)
    }
}