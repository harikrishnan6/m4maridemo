package com.marriage.m4matri.ui.base

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.viewbinding.ViewBinding
import com.marriage.m4matri.network.RemoteDataSource
import com.marriage.m4matri.repository.BaseRepository
import com.marriage.m4matri.userpreferences.UserPreferences
import com.marriage.m4matri.viewModel.ViewModelFactory

abstract class BaseActivity<VM : ViewModel, R : BaseRepository, B : ViewBinding> : AppCompatActivity()
{

    protected lateinit var binding: B

    abstract fun getViewModel(): Class<VM>

    abstract fun getFragmentRepository(): R

    protected val remoteDataSource = RemoteDataSource()

    protected lateinit var viewModel:VM

    protected lateinit var userPreferences: UserPreferences


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = getViewBinding()
        userPreferences=UserPreferences(applicationContext)
        val factory=ViewModelFactory(getFragmentRepository())
        viewModel= ViewModelProvider(this,factory)[getViewModel()]
        setContentView(binding.root)

    }

    abstract fun getViewBinding(): B
}