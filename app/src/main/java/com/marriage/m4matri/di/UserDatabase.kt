package com.marriage.m4matri.di

import androidx.room.Database
import androidx.room.RoomDatabase
import com.marriage.m4matri.responses.ProfileData
import com.marriage.m4matri.responses.RemoteKeys
import com.marriage.m4matri.responses.UserList

/**
 * For creating the database
 */
@Database(version = 1, entities = [UserList::class, RemoteKeys::class], exportSchema = false)
abstract class UserDatabase : RoomDatabase() {

    abstract fun getProfileDao(): ProfileDao

    abstract fun getRemoteKeyDao(): RemoteKeyDao

}